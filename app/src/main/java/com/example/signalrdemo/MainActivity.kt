package com.example.signalrdemo

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import microsoft.aspnet.signalr.client.Platform
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent
import microsoft.aspnet.signalr.client.hubs.HubConnection
import microsoft.aspnet.signalr.client.hubs.HubProxy
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport
import java.util.concurrent.ExecutionException

class MainActivity : AppCompatActivity() {

    lateinit var mHubConnection: HubConnection
    lateinit var mHubProxy: HubProxy
    lateinit var mHandler: Handler

    var connectionID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        connect_btn.setOnClickListener {
            Platform.loadPlatformComponent(AndroidPlatformComponent())
            val serverUrl = "https://demo-nursing-chat.neusol.com/signalr/hubs"
            mHubConnection = HubConnection(serverUrl)
            val SERVER_HUB_CHAT_NAME = "signalRChatHub"
            mHubProxy = mHubConnection.createHubProxy(SERVER_HUB_CHAT_NAME)
            val clientTransport =  ServerSentEventsTransport(mHubConnection.logger)
            val signalRFuture = mHubConnection.start(clientTransport)
            try {
                signalRFuture.get()
            }catch (e: ExecutionException){
                Log.e("SimpleSignalR", e.toString())
            }catch (e: InterruptedException ){
                Log.e("SimpleSignalR", e.toString())
            }

            Log.e("Connection",mHubConnection.connectionData)
        }

        send_message_btn.setOnClickListener {
            mHubProxy.invoke("send_PrivateMessage", arrayOf(""))
            val CLIENT_METHOD_BROADAST_MESSAGE = "Testing From Android"
            mHubProxy.on(CLIENT_METHOD_BROADAST_MESSAGE, {
                mHandler.post {
                    Toast.makeText(this, it.toString(), Toast.LENGTH_SHORT).show()
                }

            },Any::class.java)
        }

        load_message_btn.setOnClickListener {

            mHubProxy.invoke("receiveMessage")
            mHubConnection.received {
                Log.e("onMessageReceived ", it.toString())

                val jsonArr = it.asJsonObject["A"].asJsonArray
            //    val senderid  = jsonArr[2].toString()
                val msgFrom = jsonArr[0].toString()

                if (msgFrom == "NewConnection") {
                    // New Connection Received

                }else if (msgFrom == "ChatHub")
                {
                    connectionID = jsonArr[1].toString()
                }else if (msgFrom == "RU") {

                }else{

                }

            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mHubConnection.stop()
    }

}